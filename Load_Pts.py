# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 10:55:50 2018

@author: Mousa Kazemi
"""

import numpy
from scipy.spatial import cKDTree
import xml.etree.cElementTree as ET
from gias2.registration import alignment_fitting as af
#from gias2.fieldwork.field import geometric_field_fitter as GFF
from gias2.fieldwork.field.tools import fitting_tools
from gias2.fieldwork.field import geometric_field



try:
    from gias2.visualisation import fieldvi
    has_mayavi = True
except ImportError:
    has_mayavi = False                            

    
    
filename_femur = '~`\Data\Femur_Bone_Points.pts'
filename_femur_cart = '~\Data\Femur_Cartilage_Points.pts'

filename_tibia = '~\Data\Tibia_Bone_Points.pts'
filename_tibia_cart = '~\Data\Tibia_Cartilage_Points.pts'

filename_fibula = '~\Data\Fibula_Bone_Points.pts'

filename_patella = '~\Data\Patella_Bone_Points.pts'
filename_patella_cart = '~\Data\Patella_Cartilage_Points.pts'

Temp_Feb_file = '~/FE_models/Template_knee_model.feb'


class ReconstructKneeModel:
    
    def __init__ (self, bone, cart, model_template):
        
        self.bone = bone
        self.cart = cart
        self.model_template = model_template
    
    def vecMag (self, v):
        V_M = numpy.sqrt(v[0]**2+v[1]**2+v[2]**2)
        self.v = V_M  
        
        return 1
                
    def bone_pts (self): 
        return numpy.loadtxt(self.bone, dtype = float)

    def cart_pts (self): 
        return numpy.loadtxt(self.cart, dtype = float)
        
        
    def write_pts_file (self):
        pts = numpy.loadtxt(self.bone, dtype = float)
        fn = self.bone.split ('.')[0]
        fn_new = fn + '.xyz'
        return numpy.savetxt (fn_new, pts)    
       
    def extract_sub_cart_pts (self):
        
        b = numpy.loadtxt(self.bone, dtype = float)
        c = numpy.loadtxt(self.cart, dtype = float)
        
        xtree = cKDTree(b)
        cd, ci = xtree.query(c, k=1) 
        
        temp = []
        for i in xrange (len(ci)):
            cart_sub = b[ci[i]]
            temp.append(cart_sub)
            
        return numpy.array(temp)

    def model_pts (self, segment):
        tree = ET.parse(self.model_template)    
        root = tree.getroot()
               
        Nodal_Coords_Temp = []             
        for child in root.findall(".//Nodes"):
        
            for i in child:
                t = i.attrib ['id']+ ','+ i.text
                t1 = [float(word) for word in t.split(',')]
                Nodal_Coords_Temp.append(t1)
                      
     
        if segment == 'model':  
            return numpy.array(Nodal_Coords_Temp)
            
        elif segment == 'femur bone':
            return numpy.array(Nodal_Coords_Temp) [38604 : 56687]
            
        elif segment == 'femur cart':
            return numpy.array(Nodal_Coords_Temp) [: 23800]
            
        elif segment == 'tibia bone':
            return numpy.array(Nodal_Coords_Temp) [56687 : 84474]
            
        elif segment == 'tibia cart':
            return numpy.array(Nodal_Coords_Temp) [23800 : 34248]
            
        elif segment == 'patella bone':
            return numpy.array(Nodal_Coords_Temp) [84474 :]
            
        elif segment == 'patella cart':
            return numpy.array(Nodal_Coords_Temp) [34248 : 38604]
        
            
    def calcNormalVectors (self, segment):
        
        nodals = self.model_pts (segment)
        normalVecs = []
        
        if segment in ['femur cart', 'tibia_cart', 'patella cart']:
            a = numpy.array(nodals)# [: 23800]
            
            art = a[(3*len (a)/4):,1:]
            sub = a [:(len (a)/4), 1:]
            for i in xrange (len(art)):
                v = art[i] - sub[i]
                v_u = v/numpy.sqrt(v[0]**2 + v[1]**2 +v[2]**2)
                normalVecs.append(v_u)
                
        return numpy.array(normalVecs)
            
    def fitQuadToPts (self, pts_quad_mesh, pts_cart, segment, scale = None):
        
        b = numpy.array(pts_quad_mesh) 
        print b[0]
        c = numpy.array(pts_cart)
        print c[0]
        normals = self.calcNormalVectors(segment)
        if scale:
            scaled_quad = [(scale* normals[i] + b[i]) for i in xrange (len(b))]
        else:
            scaled_quad = b
            
        xtree = cKDTree(b)
        cd, ci = xtree.query(c, k=1) 

        projected_mesh = []
        for i in xrange (len(ci)):
#            cart_sub = b[ci[i]]
            dif = c[ci[i]] - scaled_quad[ci[i]]
            dif_mag = self.vecMag(dif)
            projected_cart_sub = numpy.abs(dif_mag)* normals[ci[i]]+ scaled_quad[ci[i]]
            projected_mesh.append(projected_cart_sub)
            
        return  numpy.array(projected_mesh)


            
Knee_1 = ReconstructKneeModel(filename_femur, filename_femur_cart, Temp_Feb_file)   
femur_bone_pts = Knee_1.bone_pts()
femur_cart_pts = Knee_1.cart_pts()
model_cart_pts = Knee_1.model_pts('femur cart')
model_cart_pts_art = model_cart_pts[(3*len (model_cart_pts)/4):,1:]
model_cart_pts_sub = model_cart_pts[:len (model_cart_pts)/4,1:]
                                    
model_bone_pts = Knee_1.model_pts('femur bone')

T, model_cart_pts_rotated, Err = af.fitDataRigidDPEP(model_cart_pts_art, femur_cart_pts, 
                            xtol=1e-5, maxfev=0, t0=[0,0,0,1.57, 0, 1.57], sample=1000, outputErrors=1)

print Err

Model_normals= Knee_1.calcNormalVectors (segment = 'femur cart') 
numpy.savetxt ('D:\Tutorials_series\ImageProcessing\Nynke\Data\Femur_cart_normals_Nynke_01.asc', Model_normals) 
Quad_deformed= Knee_1.fitQuadToPts (model_cart_pts_rotated, femur_cart_pts, segment = 'femur cart', scale = 1.2)  

### Read geometric field
# filenames
gf_file_fem = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Femur_bone_Nynke_01.geof'
ens_file_fem = 'D:\PCA_3\Input\models\\femur_right.ens'
mesh_file_fem = 'D:\PCA_3\Input\models\\femur_right.mesh'
pc_file_fem = 'D:\Tutorials_series\ImageProcessing\MRT\Subj_01\Flx_0deg\Loaded\Im_edges\Segmented\Femur\PCs\Femur_right_2012-05-14_July2011TS_2_AB_rigid.pc'
gf_fem = geometric_field.load_geometric_field(gf_file_fem, ens_file_fem, mesh_file_fem)
gf_fem_eval = geometric_field.makeGeometricFieldEvaluatorSparse(gf_fem, [8,8])


# Visualisation                
V = fieldvi.Fieldvi()  
V.displayGFNodes = False 
discretisation = [8,8]
V.GFD = discretisation 

V.addData('femur cart segment', femur_cart_pts, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,1,0.5)})

V.addData('model_{}_art_rotated'.format('femur cart'), model_cart_pts_rotated, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(0,1,0)})
V.addData('model_{}_art_rotated_deformed'.format('femur cart'), Quad_deformed, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,0.5)})

V.addData('model_{}_art'.format('femur cart'), numpy.array(model_cart_pts_art), renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,0.2)})
V.addData('model_{}_sub'.format('femur cart'), numpy.array(model_cart_pts_sub), renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(0.2,0,1)})


V.addGeometricField('gf_fem', gf_fem, gf_fem_eval, [8,8])


V.configure_traits()
V.scene.background=(1,1,1)

