import numpy
from gias2.learning import PCA
from gias2.fieldwork.field.tools import mesh_fitter as mf
from gias2.musculoskeletal import fw_model_landmarks as fml
from gias2.fieldwork.field import geometric_field

try:
    from gias2.visualisation import fieldvi
    has_mayavi = True
except ImportError:
    has_mayavi = False
    
from copy import deepcopy

#segments = ['tibia', 'tibia', 'patella']

# filenames
gf_filename = 'D:\PCA_3\Input\geof_files\\tibia_fibula_01.geof'
ens_filename = 'D:\PCA_3\Input\models\\tibia_fibula_right.ens'
mesh_filename = 'D:\PCA_3\Input\models\\tibia_fibula_right.mesh'
pc_filename = 'C:\Users\Mousa Kazemi\OneDrive\ABI_home_files\MAP_Project\MAP\Data\Paper_1_WFs\\01_HR\\tibia_reconstruction\\pcs\\tibia_fibula_cubic_left_mirrored_to_right_rigid_LLP26.pc'

data_filename_tibia = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Tibia_Bone_Points.pts'
data_filename_febula = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Fibula_Bone_Points.pts'

# load data
data_1 = numpy.loadtxt(data_filename_tibia)
data_2 = numpy.loadtxt(data_filename_febula)
data = numpy.vstack((data_1, data_2))
#data = data*[1,1,-1]

# load gf
gf = geometric_field.load_geometric_field(gf_filename, ens_filename, mesh_filename)
gf_unfitted = deepcopy(gf)
  
# load PCs
shape_model = PCA.loadPrincipalComponents(pc_filename)





# since data is only of the proximal tibia, we should make our data match
source_data_proximal_tibia = gf.evaluate_geometric_field_in_elements([10,10], [24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45]).T

# now align our proximal tibia data to the target
proximal_t0 = data_1.mean(0) - source_data_proximal_tibia.mean(0)
#proximal_r0 = numpy.array([numpy.pi, 0, 0])
proximal_r0 = numpy.array([0, 0, 0])
proximal_T0 = numpy.hstack([proximal_t0, proximal_r0])
proximal_T_opt, aligned_data_proximal_tibia = mf.fitting_tools.fitDataRigidDPEP(
	source_data_proximal_tibia, data_1, t0=proximal_T0
	)

# apply the transformation above to the gf, remember that the transformation
# was about the CoM of the source data cloud
gf.transformRigidRotateAboutP(proximal_T_opt, source_data_proximal_tibia.mean(0))
gf_transformed = deepcopy(gf)

# before doing the PCFit, we must figure out the rigid-body transform between the
# average tibia and gf_transformed. A quick way to do this is to register the mean
# nodes to the nodes of gf_transformed
pc_T0, aligned_mean_nodes = mf.fitting_tools.fitRigid(
	shape_model.getMean().reshape([3,-1]).T,
	gf_transformed.get_all_point_positions(),
	xtol=1e-6,
	)

# create fitter
fitter = mf.MeshFitter('fit_example')
fitter.setData(data)
fitter.setTemplateMesh(gf)

# do pc fit
fitter.PCFObjMode = 'DPEP' # because the data is smaller than the mesh
fitter.PCFEPD = [20,20]
fitter.PCFInitTrans = pc_T0[:3] # initial translation
fitter.PCFInitRot = pc_T0[3:] # initial rotation
fitter.PCFitMaxIt = 20
fitter.PCFitmW = 0.1
fitter.PCFitXtol = 1e-6
fitter.PCFitNModes = [1,2,3]

pcfitRMSE = fitter.pcFit(shape_model)
gf_fitted = fitter.templateGF

gf_fitted.save_geometric_field(filename = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Tibia_fibula_Nynke_01.geof')
F_N, F_E = gf_fitted.triangulate((22,22), merge=True)

# Visualisation                
V = fieldvi.Fieldvi()  # instantiate visualiser
V.displayGFNodes = False  # hide mesh nodes
discretisation = [8,8]
V.GFD = discretisation # element discretisation

V.addData(
	'tibia pts', data,
	renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,0)}
	)
V.addData(
	'proximal gf', source_data_proximal_tibia,
	renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,0)}
	)
V.addData(
	'aligned proximal gf', aligned_data_proximal_tibia,
	renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,0)}
	)
V.addData(
	'aligned mean nodes', aligned_mean_nodes,
	renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,0)}
	)
gf_eval = geometric_field.makeGeometricFieldEvaluatorSparse(gf, [8,8])
V.addGeometricField('gf_unfitted', gf_unfitted, gf_eval, [8,8])
V.addGeometricField('gf_transformed', gf_transformed, gf_eval, [8,8])
V.addGeometricField('gf_fitted', gf_fitted, gf_eval, [8,8])

V.configure_traits()
V.scene.background=(0,0,0)




