# -*- coding: utf-8 -*-
"""
Created on Tue Jun 19 13:11:19 2018

@author: Mousa Kazemi
"""
import numpy
from scipy.spatial import cKDTree
import matplotlib.pyplot as plt
from gias2.fieldwork.field import geometric_field

try:
    from gias2.visualisation import fieldvi
    has_mayavi = True
except ImportError:
    has_mayavi = False




def mag (vector):
    vector = numpy.array(vector)
    m = numpy.sqrt((vector**2).sum(axis = 0))

    #m = numpy.sqrt((vector[0]**2+ vector[1]**2+vector[2]**2))
    return m
    
#def unitVec(vector):
#    vector = numpy.array(vector)
#    m = numpy.sqrt((vector**2).sum(axis = 0))
#    u = vector/m 
#    return u

def read_pts (pts): 
    return numpy.loadtxt(pts, dtype = float)
        
def unitVec(vector):
    """ Returns the unit vector of the vector.  """
    return vector / numpy.linalg.norm(vector)  # norm is similar to mag(v) here
    
def load_material_points(filename):
    
    x = numpy.loadtxt(filename)
    Fem_Mat_xi = [(int(row[0]), row[1:]) for row in x]
    return Fem_Mat_xi
    
def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unitVec(v1)
    v2_u = unitVec(v2)
    return numpy.degrees(numpy.arccos(numpy.clip(numpy.dot(v1_u, v2_u), -1.0, 1.0)) )
    
def extract_sub_cart_pts (bone, cart):
    
    b = numpy.loadtxt(bone, dtype = float)
    c = numpy.loadtxt(cart, dtype = float)
    
    xtree = cKDTree(b)
    cd, ci = xtree.query(c, k=1) 
    
    temp = []
    for i in xrange (len(ci)):
        cart_sub = b[ci[i]]
        temp.append(cart_sub)
        
    return numpy.array(temp)

    
def extract_cartilage_thickness (Cart_art_pts, Cart_sub_pts, Fem_Mat_coords_1, Fem_Mat_normals ):
    
    num_cls_pts = 10
    Fem_cart_coords = Cart_art_pts # numpy.loadtxt(Cart_art_pts, dtype = float)
    Sub_Fem_cart_coords = Cart_sub_pts # numpy.loadtxt(Cart_sub_pts, dtype = float)
    
    xtree = cKDTree(Fem_cart_coords)
    cd, ci = xtree.query(Fem_Mat_coords_1, k=num_cls_pts) # cd is an array of the distance from each point in material points(Y) to its closest point in cartilage point cloud (X)
    							  #	and ci is an array of the indices of the closest points in X to each point in Y
    xtree_1 = cKDTree(Sub_Fem_cart_coords)
    cd_1, ci_1 = xtree_1.query(Fem_Mat_coords_1, k=num_cls_pts)
    #print numpy.shape(cd)
    #print numpy.shape(ci)
    #print numpy.shape(cd_1)
    #print numpy.shape(ci_1)
    
    # calculate the cartilage thickness along the normal of material points
    Angle_cart_wrt_normal = []
    Angle_bone_wrt_normal = []
    mag_sum_cl = []
    mag_sum_pjt = []
    Fem_cart_thickness = []
    Fem_cart_thickness_2 = []
    
    for i in range(0, len(Fem_Mat_coords_1)): 
        t1 = 0
        t2 = 0                
        for j in range (0, num_cls_pts):
            a = Fem_Mat_coords_1[i]
            b = Fem_cart_coords[ci[i][j]]
            c = Sub_Fem_cart_coords[ci_1[i][j]]
            v = b-a  #closest points vector of articular cartilage point
            v_1 = c-a #closest points vector of sub cartilage point
            
            if angle_between(v, Fem_Mat_normals[i]) < 45:
                
                v_projected = (numpy.dot(v, Fem_Mat_normals[i]) /(mag(Fem_Mat_normals[i]))**2)* Fem_Mat_normals[i]# cartilage thickness (projected vector of the closest points to the normal vector)
    #                v_projected = (numpy.dot(v, Fem_Mat_normals[i]) /(mag(Fem_Mat_normals[i]))**2)* Fem_Mat_normals[i]# cartilage thickness (projected vector of the closest points to the normal vector)
                Angle_cart_wrt_normal.append(angle_between(v, Fem_Mat_normals[i]))
                    
                v_1_projected = (numpy.dot(v_1, Fem_Mat_normals[i]) /(mag(Fem_Mat_normals[i]))**2)* Fem_Mat_normals[i] # cartilage thickness (projected vector of the closest points to the normal vector)
                Angle_bone_wrt_normal.append(angle_between(v_1, Fem_Mat_normals[i]))
                mag_sum_cl.append(numpy.linalg.norm(numpy.subtract(v, v_1)))
                
                mag_sum_pjt.append(numpy.linalg.norm(numpy.subtract(v_projected, v_1_projected)))
                
                diff = numpy.abs(mag(v)-mag(v_1)) # actual thickness based on MRI data
                diff_2 =  numpy.abs(mag(v_projected)-mag(v_1_projected))
                
                #print 'diff_figure ....... :', diff
    #                        Fem_cart_thickness.append(diff)
    #                        Fem_cart_thickness_2.append(diff_2) 
#                break
    #                        
    #                        pass                                 
            elif angle_between(v, Fem_Mat_normals[i]) >= 45:
                v_projected = (numpy.dot(v, Fem_Mat_normals[i]) /(mag(Fem_Mat_normals[i]))**2)* Fem_Mat_normals[i]# cartilage thickness (projected vector of the closest points to the normal vector)
    #                v_projected = (numpy.dot(v, Fem_Mat_normals[i]) /(mag(Fem_Mat_normals[i]))**2)* Fem_Mat_normals[i]# cartilage thickness (projected vector of the closest points to the normal vector)
                Angle_cart_wrt_normal.append(angle_between(v, Fem_Mat_normals[i]))
                    
                v_1_projected = (numpy.dot(v_1, Fem_Mat_normals[i]) /(mag(Fem_Mat_normals[i]))**2)* Fem_Mat_normals[i] # cartilage thickness (projected vector of the closest points to the normal vector)
                Angle_bone_wrt_normal.append(angle_between(v_1, Fem_Mat_normals[i]))
                mag_sum_cl.append(numpy.linalg.norm(numpy.subtract(v, v_1)))
                
                mag_sum_pjt.append(numpy.linalg.norm(numpy.subtract(v_projected, v_1_projected)))
                
                t1 += numpy.abs(mag(v)-mag(v_1)) # actual thickness based on MRI data
                t2 +=  numpy.abs(mag(v_projected)-mag(v_1_projected))
                diff_2 = t2/j+1
                diff = t1/j+1
                #print 'diff_figure ....... :', diff
        Fem_cart_thickness.append(diff)
        Fem_cart_thickness_2.append(diff_2) 
            
    print   'shape of Fem_cart_thickness_2: ', numpy.shape(Fem_cart_thickness_2)      
    count = 0        
    for i in xrange (len(Fem_cart_thickness_2)): 
        
        if numpy.isinf(Fem_cart_thickness_2[i]):
            
            Fem_cart_thickness_2[i] = Fem_cart_thickness_2[i-1] #numpy.mean(Fem_cart_thickness_2)
            count+=1
            
        elif numpy.isinf(Fem_cart_thickness[i]):
            
            Fem_cart_thickness[i] = Fem_cart_thickness[i-1] #numpy.mean(Fem_cart_thickness_2)
        
        else:
            pass
            
    print 'number of points with Inf or NaN: {}'.format(count)  
    
    itera = 1
    while itera < 10:
        print 'iteration: ', itera
        print 'mean: ', numpy.mean(Fem_cart_thickness_2)
        print 'std: ', numpy.std(Fem_cart_thickness_2)
        print 'min: ', numpy.min(Fem_cart_thickness_2)
        print 'max: ', numpy.max(Fem_cart_thickness_2) , '\n'
        
        for i in xrange (len(Fem_cart_thickness_2)): 

            if Fem_cart_thickness_2[i] >= numpy.max(Fem_cart_thickness_2)- numpy.std(Fem_cart_thickness_2) or Fem_cart_thickness_2[i] <= numpy.min(Fem_cart_thickness_2)+ numpy.std(Fem_cart_thickness_2):
                
                Fem_cart_thickness_2[i] = Fem_cart_thickness_2[i-1]#+ Fem_cart_thickness_2[i-5])/2#numpy.mean(Fem_cart_thickness_2)
          
            else:
                pass
            
        itera +=1
#    if Fem_cart_thickness_2[i] > numpy.max(Fem_cart_thickness_2)- numpy.mean(Fem_cart_thickness_2): 
#        Fem_cart_thickness_2[i] = numpy.max(Fem_cart_thickness_2)- numpy.mean(Fem_cart_thickness_2)
                     
        
    Fem_cart_nodals = []
    Fem_cart_nodals_2 = []
    for i in xrange (len(Fem_Mat_coords_1)):
#                nodals = Fem_Mat_coords_1[i]+(unitVec(Fem_Mat_coords_1[i])*Fem_cart_thickness[i])
        nodals = Fem_Mat_coords_1[i]+(unitVec(Fem_Mat_normals[i])*Fem_cart_thickness[i])
        nodals_2 = Fem_Mat_coords_1[i]+(unitVec(Fem_Mat_normals[i])*Fem_cart_thickness_2[i])
        Fem_cart_nodals.append(nodals)
        Fem_cart_nodals_2.append(nodals_2)
        
    Fem_cart_nodals = numpy.array(Fem_cart_nodals)
    Fem_cart_nodals_2 = numpy.array(Fem_cart_nodals_2)

    return count, Fem_cart_thickness_2 , Fem_cart_nodals_2
    
# data 

filename_femur = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Femur_Bone_Points.pts'
filename_femur_cart = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Femur_Cartilage_Points.pts'


filename_tibia = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Tibia_Bone_Points.pts'
filename_tibia_cart = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Tibia_Cartilage_Points.pts'

filename_fibula = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Fibula_Bone_Points.pts'

filename_patella = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Patella_Bone_Points.pts'
filename_patella_cart = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Patella_Cartilage_Points.pts'

#### femur    
# filenames
gf_file_fem = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Femur_bone_Nynke_01.geof'
ens_file_fem = 'D:\PCA_3\Input\models\\femur_right.ens'
mesh_file_fem = 'D:\PCA_3\Input\models\\femur_right.mesh'

gf_fem = geometric_field.load_geometric_field(gf_file_fem, ens_file_fem, mesh_file_fem)
gf_fem_eval = geometric_field.makeGeometricFieldEvaluatorSparse(gf_fem, [8,8])

fem_bone_nodes, fem_bone_elements = gf_fem.triangulate((22,22), merge=True)
fem_bone_nodes_xyz = numpy.savetxt ('D:\Tutorials_series\ImageProcessing\Nynke\Data\Femur_nodes_Nynke_01.xyz', fem_bone_nodes)

fem_cart_att_Mat_pts_file = 'D:\HostFitting\Outputs\HR02\FemurCartMpt_HR02.asc'
fem_Mat_xi_1 = load_material_points(fem_cart_att_Mat_pts_file)
fem_MPTS = gf_fem.evaluate_geometric_field_at_element_points_3(fem_Mat_xi_1).T
            
# evaluate normals at material points
fem_Mat_normals = []
for elem, xi in fem_Mat_xi_1:
    fem_Mat_normals.append(gf_fem.evaluate_normal_at_element_point(elem, xi))
fem_normals = numpy.array(fem_Mat_normals)
#fem_normals_1 = numpy.loadtxt ('D:\Tutorials_series\ImageProcessing\Nynke\Data\Femur_cart_normals_Nynke_01.asc', dtype = float) 


fem_art_pts = numpy.loadtxt (filename_femur_cart, dtype = float)
fem_sub_pts = extract_sub_cart_pts (filename_femur, filename_femur_cart)

C1, A1, B1 = extract_cartilage_thickness (fem_art_pts, fem_sub_pts, fem_MPTS, fem_normals ) # femur

plt.hist(A1, bins='auto')
plt.show()

numpy.savetxt('D:\Tutorials_series\ImageProcessing\Nynke\Outputs\Femur_cart_thickness_Nynke_01.asc', A1)
numpy.savetxt('D:\Tutorials_series\ImageProcessing\Nynke\Outputs\Femur_art_cart_pts_Nynke_01.asc', B1)

#### tibia

gf_file_tib = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Tibia_fibula_Nynke_01.geof'
ens_file_tib = 'D:\PCA_3\Input\models\\tibia_fibula_right.ens'
mesh_file_tib = 'D:\PCA_3\Input\models\\tibia_fibula_right.mesh'

gf_tib = geometric_field.load_geometric_field(gf_file_tib, ens_file_tib, mesh_file_tib)
gf_tib_eval = geometric_field.makeGeometricFieldEvaluatorSparse(gf_tib, [8,8])
tib_bone_nodes, tib_bone_elements = gf_tib.triangulate((22,22), merge=True)
tib_bone_nodes_xyz = numpy.savetxt ('D:\Tutorials_series\ImageProcessing\Nynke\Data\Tibia_fibula_nodes_Nynke_01.xyz', tib_bone_nodes)
#
tib_cart_att_Mat_pts_file = 'D:\HostFitting\Inputs\MeshMaterialPoints\Tib_SubCart_Mpts_SideExtruded.asc'
tib_Mat_xi_1 = load_material_points(tib_cart_att_Mat_pts_file)
tib_MPTS = gf_tib.evaluate_geometric_field_at_element_points_3(tib_Mat_xi_1).T

# evaluate normals at material points
tib_Mat_normals = []
for elem, xi in tib_Mat_xi_1:
    tib_Mat_normals.append(gf_tib.evaluate_normal_at_element_point(elem, xi))
tib_normals = numpy.array(tib_Mat_normals)

tib_art_pts = numpy.loadtxt (filename_tibia_cart, dtype = float)
tib_sub_pts = extract_sub_cart_pts (filename_tibia, filename_tibia_cart)
       
C2, A2, B2 = extract_cartilage_thickness (tib_art_pts, tib_sub_pts, tib_MPTS, tib_normals ) # Tibia

numpy.savetxt('D:\Tutorials_series\ImageProcessing\Nynke\Outputs\Tibia_cart_thickness_Nynke_01.asc', A2)
numpy.savetxt('D:\Tutorials_series\ImageProcessing\Nynke\Outputs\Tibia_art_cart_pts_Nynke_01.asc', B2)


plt.hist(A2, bins='auto')
plt.show()



#### patella
gf_file_pat = 'D:\Tutorials_series\ImageProcessing\Nynke\Data\Patella_bone_Nynke_01.geof'
ens_file_pat = 'D:\PCA_3\Input\models\\patella_right.ens'
mesh_file_pat = 'D:\PCA_3\Input\models\\patella_right.mesh'

gf_pat = geometric_field.load_geometric_field(gf_file_pat, ens_file_pat, mesh_file_pat)
gf_pat_eval = geometric_field.makeGeometricFieldEvaluatorSparse(gf_pat, [8,8])


pat_bone_nodes, pat_bone_elements = gf_pat.triangulate((22,22), merge=True)
pat_bone_nodes_xyz = numpy.savetxt ('D:\Tutorials_series\ImageProcessing\Nynke\Data\Patella_nodes_Nynke_01.xyz', pat_bone_nodes)
 

pat_cart_att_Mat_pts_file = 'D:\HostFitting\Inputs\MeshMaterialPoints\PatellaSubCartMpts.asc'
pat_Mat_xi_1 = load_material_points(pat_cart_att_Mat_pts_file)
pat_MPTS = gf_pat.evaluate_geometric_field_at_element_points_3(pat_Mat_xi_1).T

# evaluate normals at material points
pat_Mat_normals = []
for elem, xi in pat_Mat_xi_1:
    pat_Mat_normals.append(gf_pat.evaluate_normal_at_element_point(elem, xi))
pat_normals = numpy.array(pat_Mat_normals)

pat_art_pts = numpy.loadtxt (filename_patella_cart, dtype = float)
pat_sub_pts = extract_sub_cart_pts (filename_patella, filename_patella_cart)
       
C3, A3, B3 = extract_cartilage_thickness (pat_art_pts, pat_sub_pts, pat_MPTS, pat_normals ) # Tibia

numpy.savetxt('D:\Tutorials_series\ImageProcessing\Nynke\Outputs\Patella_cart_thickness_Nynke_01.asc', A3)
numpy.savetxt('D:\Tutorials_series\ImageProcessing\Nynke\Outputs\Patella_art_cart_pts_Nynke_01.asc', B3)


plt.hist(A3, bins='auto')
plt.show()

# Visualisation                
V = fieldvi.Fieldvi()  
V.displayGFNodes = False 
discretisation = [8,8]
V.GFD = discretisation 


V.addData('fem_art_pts', fem_art_pts, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(0,1,0)})
V.addData('fem_sub_pts', fem_sub_pts, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(0,1,1)})

V.addData('Fem_MPTS', fem_MPTS, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,1)})
V.addData('B1', numpy.array(B1), renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,0.5)})
#
#
V.addGeometricField('gf_fem', gf_fem, gf_fem_eval, [8,8])

V.addData('tib_art_pts', tib_art_pts, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(0,1,0)})
V.addData('tib_sub_pts', tib_sub_pts, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(0,1,1)})

V.addData('tib_MPTS', tib_MPTS, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,1)})
V.addData('B2', numpy.array(B2), renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,0.5)})


V.addGeometricField('gf_tib', gf_tib, gf_tib_eval, [8,8])



V.addData('pat_art_pts', pat_art_pts, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(0,1,0)})
V.addData('pat_sub_pts', pat_sub_pts, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(0,1,1)})

V.addData('pat_MPTS', pat_MPTS, renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,1)})
V.addData('B3', numpy.array(B3), renderArgs={'mode':'sphere', 'scale_factor':1.0, 'color':(1,0,0.5)})


V.addGeometricField('gf_pat', gf_pat, gf_pat_eval, [8,8])

V.configure_traits()
V.scene.background=(1,1,1)
